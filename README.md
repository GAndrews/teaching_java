Java Training Course for technical non-programmers.
======

Hi everyone, I've put together this course in the hopes that it can help 
those that use computers but don't have experience in the programming side learn
a little about java and hopefully spark an interest in development.

You can [start the sessions here](https://gitlab.com/GAndrews/teaching_java/wikis/home)

If you've got any questions or issues with the course then please don't hesitate
to contact me through either through the issues section or by emailing me at 
["you've got a problem"](email:incoming+GAndrews/teaching_java@gitlab.com)

I hope you enjoy the course and find it useful,

Gareth