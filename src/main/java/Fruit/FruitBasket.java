public class FruitBasket {
    public static void main( String[] args) {
        Fruit fruitOrange = new Orange(); //Because Oranges are Fruit, they are able to fit into the description of a Fruit
        Orange orange = new Orange();
        orange.describe();
    }
}