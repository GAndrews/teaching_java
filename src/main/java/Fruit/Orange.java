


    public class Orange extends Fruit {
        Orange() {
            super("Orange", "orange"); //Super calls the parents version of a method, in this case the constructor
        }

        public void describe() {
            super.describe(); //This runs the parent classes version of describe
            System.out.println("<-------------------------->");
            System.out.println("Hello, I am " + name);
            System.out.println("I am coloured: " + colour);
        }
    }