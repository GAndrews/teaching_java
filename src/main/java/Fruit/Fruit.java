     public class Fruit { 
        public String name;
        public String colour;
        
        public Fruit( String name, String colour) {
            this.name = name;
            this.colour = colour;
        } 

        public void describe() { System.out.printf("My name is %s and I'm %s'", name, colour); }
    }