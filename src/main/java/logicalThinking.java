class logicalThinking {

    public static void main(String[] args) {
       if("hello".equals("HELLO".toLowerCase())) {
            say( helloTo( args[0]));
       }
    }
    
    private static String helloTo(String name) {
        return "Hello " + name;
    }
 
   private static void say(String message) {
      System.out.println( message);
   }
}