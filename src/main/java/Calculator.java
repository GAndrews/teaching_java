import java.util.Scanner;

public class Calculator {
    private int currentValue = 0;

    public static void main(String[] args) {
        Calculator calc = new Calculator();
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.println("Please select a function");
            System.out.println(" 0 - Show Results" );
            System.out.println(" 1 - Add" );
            System.out.println(" 2 - Subtract" );
            System.out.println(" 3 - Multiply" );
            System.out.println(" 4 - Divide" );
            System.out.println(" 5 - Set Number" );
            int selection = 0;
            selection = sc.nextInt();

            System.out.println("Please enter a number: ");
            int value = 0;
            value = sc.nextInt();
            switch (selection) {
                case 0:
                    System.out.println( calc.currentValue);
                    break;
                case 1:
                    System.out.println( "Added: " + value);
                    break;
                case 2:
                    System.out.println("Subtracted: " + value);
                    break;
                case 3:
                    System.out.println("Multiplied: " + value);
                    break;
                case 4:
                    System.out.println( "Divided : " + value);
                    break;
                case 5:
                    System.out.println( "Number set to : " + value);
                    break;
            }
        }
    }

    public void setNumber (int value) {
        currentValue = 0;
    }

    public void add (int number) {
        currentValue += number;     //The += is the same as saying currentValue = currentValue + number
    }

    public void subtract (int number) { currentValue -= number; }

    public void multiply (int number) { number *= currentValue; }

    public void divide (int number) { currentValue /= number; }
    public int getValue () { return currentValue; }

    //Can you see anything wrong with this method? There are two issues here, one code, one professional
    public int clearValue() { currentValue = currentValue; return currentValue; } 
}
