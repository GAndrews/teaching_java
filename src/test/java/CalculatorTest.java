import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*; //The .* notation informs it that we're importing everything under org.junit.Assert

/**
 * This is a Java Doc (noted by the double ** after the slash.
 * These can be compiled into HTML documents to help outline and update users about the code.
 * its considered good practise to Java Doc every class and methods which are complex or contain business logic.
 *
 * Calculator Test is designed to test the functionlity and accuracy of Calculator.
 *
 * Tests are all passing, but does that mean they're right?
 */
public class CalculatorTest {
    Calculator calculator = new Calculator();

    @Test
    public void test1_testSetNumber_One() {
        calculator.setNumber( 2);
        assertEquals(calculator.getValue(), 0); //Does the order of these matter?
    }

    @Test
    public void test2_testClear() {
        int value = calculator.clearValue();
        assertEquals( "Broken", calculator.getValue(), 0); //Is the message useful? can you think of a better one
    }

    @Test
    public void test3_testAdd_addThree() {
        calculator.add(3);
        assertEquals( calculator.getValue(), 3);
    }

    @Test
    public void test4_testMultiply() {
        Calculator calc = new Calculator();
        calc.setNumber(10);
        calc.divide(2);
        assertEquals( 0, calc.getValue());
    }
}
